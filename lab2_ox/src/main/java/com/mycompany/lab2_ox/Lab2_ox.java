/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2_ox;

/**
 *
 * @author informatics
 */
import java.util.Scanner;

public class Lab2_ox {

    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
    static char Player = 'X';
    static int row, col;

    static void PWelcome() {
        System.out.println("Welcome to OX");
    }

    static void PTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    static void PTurn() {
        System.out.println(Player + " Turn");
    }

    static void inputrc() {
        Scanner rc = new Scanner(System.in);
        System.out.print("Please input row,col : ");
        row = rc.nextInt();
        col = rc.nextInt();
        table[row - 1][col - 1] = Player;
    }
    static void switchP(){
        if(Player=='X'){
            Player = 'O';
        }
        else{
            Player = 'X';
        }
    }
    public static void main(String[] args) {
        PWelcome();
        while (true) {
            PTable();
            PTurn();
            inputrc();
            switchP();
        }

    }
}
